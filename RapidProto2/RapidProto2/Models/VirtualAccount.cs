﻿using System;
using System.Collections.Generic;

namespace RapidProto2.Models
{
    public class VirtualAccount
    {
        public int ID { get; set; }
        public int virtualmoney { get; set; }
        public int bitcoin { get; set; }
        public int ethereum { get; set; }
    }
}
