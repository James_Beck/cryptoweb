﻿using RapidProto2.Models;
using System;
using System.Linq;

namespace RapidProto2.Data
{
    public class DbInitializer
    {
        public static void Initialize(InvestmentContext context)
        {
            context.Database.EnsureCreated();

            if (context.VirtualAccounts.Any())
            {
                return; //We've got information in the account
            }

            var virtualaccount = new VirtualAccount[]
            {
                new VirtualAccount{virtualmoney=25000, bitcoin=0, ethereum=0}
            };
            
            foreach (VirtualAccount v in virtualaccount)
            {
                context.VirtualAccounts.Add(v);
            }
            context.SaveChanges();
        }
    }
}
