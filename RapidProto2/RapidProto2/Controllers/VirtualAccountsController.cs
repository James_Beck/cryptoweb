﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RapidProto2.Data;
using RapidProto2.Models;

namespace RapidProto2.Controllers
{
    public class VirtualAccountsController : Controller
    {
        private readonly InvestmentContext _context;

        public VirtualAccountsController(InvestmentContext context)
        {
            _context = context;
        }

        // GET: VirtualAccounts
        public async Task<IActionResult> Index()
        {
            return View(await _context.VirtualAccounts.ToListAsync());
        }

        // GET: VirtualAccounts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var virtualAccount = await _context.VirtualAccounts
                .SingleOrDefaultAsync(m => m.ID == id);
            if (virtualAccount == null)
            {
                return NotFound();
            }

            return View(virtualAccount);
        }

        // GET: VirtualAccounts/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: VirtualAccounts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,virtualmoney,bitcoin,ethereum")] VirtualAccount virtualAccount)
        {
            if (ModelState.IsValid)
            {
                _context.Add(virtualAccount);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(virtualAccount);
        }

        // GET: VirtualAccounts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var virtualAccount = await _context.VirtualAccounts.SingleOrDefaultAsync(m => m.ID == id);
            if (virtualAccount == null)
            {
                return NotFound();
            }
            return View(virtualAccount);
        }

        // POST: VirtualAccounts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,virtualmoney,bitcoin,ethereum")] VirtualAccount virtualAccount)
        {
            if (id != virtualAccount.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(virtualAccount);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VirtualAccountExists(virtualAccount.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(virtualAccount);
        }

        // GET: VirtualAccounts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var virtualAccount = await _context.VirtualAccounts
                .SingleOrDefaultAsync(m => m.ID == id);
            if (virtualAccount == null)
            {
                return NotFound();
            }

            return View(virtualAccount);
        }

        // POST: VirtualAccounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var virtualAccount = await _context.VirtualAccounts.SingleOrDefaultAsync(m => m.ID == id);
            _context.VirtualAccounts.Remove(virtualAccount);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool VirtualAccountExists(int id)
        {
            return _context.VirtualAccounts.Any(e => e.ID == id);
        }
    }
}
