﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RapidProto3.Data;
using RapidProto3.Models;

namespace RapidProto3.Controllers
{
    public class InvestmentAccountsController : Controller
    {
        private readonly InvestmentContext _context;

        public InvestmentAccountsController(InvestmentContext context)
        {
            _context = context;
        }

        // GET: InvestmentAccounts
        public async Task<IActionResult> Index()
        {
            return View(await _context.InvestmentAccounts.ToListAsync());
        }

        // GET: InvestmentAccounts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var investmentAccount = await _context.InvestmentAccounts
                .SingleOrDefaultAsync(m => m.ID == id);
            if (investmentAccount == null)
            {
                return NotFound();
            }

            return View(investmentAccount);
        }

        // GET: InvestmentAccounts/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: InvestmentAccounts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Username,Password,VirtualMoney,VirtualBitcoin,VirtualEthereum")] InvestmentAccount investmentAccount)
        {
            if (ModelState.IsValid)
            {
                _context.Add(investmentAccount);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(investmentAccount);
        }

        // GET: InvestmentAccounts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var investmentAccount = await _context.InvestmentAccounts.SingleOrDefaultAsync(m => m.ID == id);
            if (investmentAccount == null)
            {
                return NotFound();
            }
            return View(investmentAccount);
        }

        // POST: InvestmentAccounts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Username,Password,VirtualMoney,VirtualBitcoin,VirtualEthereum")] InvestmentAccount investmentAccount)
        {
            if (id != investmentAccount.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(investmentAccount);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!InvestmentAccountExists(investmentAccount.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(investmentAccount);
        }

        // GET: InvestmentAccounts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var investmentAccount = await _context.InvestmentAccounts
                .SingleOrDefaultAsync(m => m.ID == id);
            if (investmentAccount == null)
            {
                return NotFound();
            }

            return View(investmentAccount);
        }

        // POST: InvestmentAccounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var investmentAccount = await _context.InvestmentAccounts.SingleOrDefaultAsync(m => m.ID == id);
            _context.InvestmentAccounts.Remove(investmentAccount);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool InvestmentAccountExists(int id)
        {
            return _context.InvestmentAccounts.Any(e => e.ID == id);
        }
    }
}
