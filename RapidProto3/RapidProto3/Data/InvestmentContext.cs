﻿using RapidProto3.Models;
using Microsoft.EntityFrameworkCore;

namespace RapidProto3.Data
{
    public class InvestmentContext : DbContext
    {
        public InvestmentContext(DbContextOptions<InvestmentContext> options) : base(options) { }

        public DbSet<InvestmentAccount> InvestmentAccounts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<InvestmentAccount>().ToTable("InvestmentAccount");
        }
    }
}
