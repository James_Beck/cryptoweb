﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RapidProto3.Models;

namespace RapidProto3.Data
{
    public static class DbInitializer
    {
        public static void Initialize(InvestmentContext context)
        {
            context.Database.EnsureCreated();

            if (context.InvestmentAccounts.Any())
            {
                return; //Database has been seeded
            }

            var defaultAccounts = new InvestmentAccount[]
            {
                new InvestmentAccount{ Username="CeeBee", Password="Password", VirtualMoney=25000, VirtualBitcoin=0, VirtualEthereum=0 },
                new InvestmentAccount{ Username="Beckintosh", Password="Password", VirtualMoney=25000, VirtualBitcoin=0, VirtualEthereum=0 },
            };

            foreach (InvestmentAccount invacc in defaultAccounts)
            {
                context.InvestmentAccounts.Add(invacc);
            }

            context.SaveChanges();

        }
    }
}
