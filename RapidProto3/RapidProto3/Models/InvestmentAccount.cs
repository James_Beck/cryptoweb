﻿using System;
using System.Collections.Generic;

namespace RapidProto3.Models
{
    public class InvestmentAccount
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int VirtualMoney { get; set; }
        public int VirtualBitcoin { get; set; }
        public int VirtualEthereum { get; set; }
    }
}
