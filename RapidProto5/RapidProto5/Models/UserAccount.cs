﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RapidProto5.Models
{
    public class UserAccount
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int Vmoney { get; set; }
        public int Bitcoin { get; set; }
        public int Ethereum { get; set; }
    }
}
