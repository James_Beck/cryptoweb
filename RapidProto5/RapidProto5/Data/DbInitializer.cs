﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RapidProto5.Models;


namespace RapidProto5.Data
{
    public class DbInitializer
    {
        public static void Initialize(InvestmentContext context)
        {
            context.Database.EnsureCreated();

            if(context.UserAccounts.Any())
            {
                return; //Database has been seeded
            }

            var useraccounts = new UserAccount[]
            {
                new UserAccount{ Username="CeeBee", Password="password", Vmoney=25000, Bitcoin=0, Ethereum=0 },
                new UserAccount{ Username="Beckintosh", Password="password", Vmoney=25000, Bitcoin=0, Ethereum=0 },
            };

            foreach (UserAccount u in useraccounts)
            {
                context.UserAccounts.Add(u);
            }

            context.SaveChanges();
        }
    }
}
